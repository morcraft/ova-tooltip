const tippy = require('tippy.js')
const styles = require('./styles.js')

module.exports = function(){
    var _t = tippy($('[title]').toArray(), {
        delay: 100,
        arrow: true,
        arrowType: 'round',
        size: 'large',
        duration: 500,
        animation: 'shift-toward',
        intertia: true,
        theme: 'ova'
    })

    var head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');
    style.type = 'text/css';
    if (style.styleSheet){
        // This is required for IE8 and below.
        style.styleSheet.cssText = styles;
    } else {
        style.appendChild(document.createTextNode(styles));
    }

    head.appendChild(style);

    return _t;
}